videojs.registerPlugin("metaData", function (title, duration, player) {
  var eventManager = function (e) {
    e.on("play", function () {
      if (e.el().querySelector(".video-meta__item--title")) {
        e.el()
          .querySelector(".video-meta__item--title")
          .classList.add("is-playing");
      }
      if (e.el().querySelector(".video-meta__item--duration")) {
        e.el()
          .querySelector(".video-meta__item--duration")
          .classList.add("is-playing");
      }
    });
    e.on("pause", function () {
      if (e.el().querySelector(".video-meta__item--title")) {
        e.el()
          .querySelector(".video-meta__item--title")
          .classList.remove("is-playing");
      }
    });
  };
  var createNode = function (className, content) {
    var returnValue = document.createElement("div");
    returnValue.textContent = content;
    returnValue.setAttribute("class", "video-meta__item " + className);
    return returnValue;
  };
  var formatVideoDuration = function (seconds) {
    var mm = Math.floor(seconds / 60) || 0;
    var ss = ("0" + Math.floor(seconds % 60)).slice(-2);
    return mm + ":" + ss;
  };
  var metaTitle = createNode("video-meta__item--title", title);
  var metaDuration = createNode(
    "video-meta__item--duration",
    formatVideoDuration(Math.floor(duration))
  );
  if (duration) {
    player.el().prepend(metaDuration);
  }
  if (title) {
    player.el().prepend(metaTitle);
  }
  eventManager(player);
});
