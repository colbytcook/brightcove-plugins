videojs.registerPlugin("closeCaptioning", function () {
  var player = this;
  var userLanguage = "";
  // However, if the `lang` attribute should be ignored, default to the browser or OS settings.  This option
  // should be used when there's no language switcher or other page translation, meaning that a user who prefers
  // German and wants the subtitles would still be looking at a page in English because no German page exists.
  if (player.el_.hasAttribute("data-use-page-lang")) {
    // Get the user's preferred language.
    // By default, use the `lang` attribute of the page or surrounding markup (player.language()
    userLanguage = document.documentElement.lang;
  } else {
    userLanguage = window.navigator.language;
  }
  userLanguage = userLanguage.substr(0, 2);

  // Display the caption in the user's language by default (if captions exist).
  var trackLanguage;
  var textTracks = player.textTracks();
  for (var i = 0; i < textTracks.length; i++) {
    trackLanguage = textTracks[i].language.substr(0, 2);
    if (textTracks[i].kind == "subtitles" && trackLanguage == userLanguage) {
      // We found a track that should be the default.
      textTracks[i].mode = "showing";
    } else {
      textTracks[i].mode = "hidden";
    }
  }
});
