videojs.registerPlugin("ga", function (options) {
  var adStateRegex,
    currentVideo,
    dataSetupOptions,
    defaultLabel,
    defaultsEventsToTrack,
    end,
    endTracked,
    error,
    eventCategory,
    eventLabel,
    eventNames,
    eventsToTrack,
    fullscreen,
    getEventName,
    isInAdState,
    loaded,
    parsedOptions,
    pause,
    percentsAlreadyTracked,
    percentsPlayedInterval,
    play,
    player,
    referrer,
    resize,
    seekEnd,
    seekStart,
    seeking,
    sendbeacon,
    sendbeaconOverride,
    start,
    startTracked,
    timeupdate,
    tracker,
    trackerName,
    volumeChange,
    getEventLabel,
    shareButtonClick,
    shareLinkClick,
    ctaClick,
    replay,
    sendRequestToLMMS,
    lmmsEndpoint,
    paused,
    _this = this;
  var __indexOf =
    [].indexOf ||
    function (item) {
      for (var i = 0, l = this.length; i < l; i++) {
        if (i in this && this[i] === item) return i;
      }
      return -1;
    };

  if (options == null) {
    options = {};
  }
  referrer = document.createElement("a");
  referrer.href = document.referrer;
  if (
    self !== top &&
    window.location.host === "preview-players.brightcove.net" &&
    referrer.hostname === "studio.brightcove.com"
  ) {
    videojs.log(
      "Google analytics plugin will not track events in Video Cloud Studio"
    );
    return;
  }
  player = this;
  dataSetupOptions = {};
  if (this.options()["data-setup"]) {
    parsedOptions = JSON.parse(this.options()["data-setup"]);
    if (parsedOptions.ga) {
      dataSetupOptions = parsedOptions.ga;
    }
  }
  defaultsEventsToTrack = [
    "player_load",
    "video_load",
    "percent_played",
    "start",
    "end",
    "seek",
    "play",
    "pause",
    "resize",
    "volume_change",
    "error",
    "fullscreen",
  ];
  eventsToTrack =
    options.eventsToTrack ||
    dataSetupOptions.eventsToTrack ||
    defaultsEventsToTrack;
  percentsPlayedInterval =
    options.percentsPlayedInterval ||
    dataSetupOptions.percentsPlayedInterval ||
    25;
  eventCategory =
    options.eventCategory ||
    dataSetupOptions.eventCategory ||
    "Brightcove Player";
  defaultLabel = options.eventLabel || dataSetupOptions.eventLabel;
  sendbeaconOverride = options.sendbeaconOverride || false;
  options.debug = options.debug || false;
  options.trackerName = options.trackerName || null;
  trackerName = "";
  if (typeof options.trackerName === "string") {
    trackerName = options.trackerName + ".";
  }
  percentsAlreadyTracked = [];
  startTracked = false;
  endTracked = false;
  seekStart = seekEnd = 0;
  seeking = false;
  paused = false;
  eventLabel = "";
  currentVideo = "";
  if (typeof window.drupalSettings != "undefined") {
    if (typeof window.drupalSettings.lmms != "undefined") {
      lmmsEndpoint =
        "/" + window.drupalSettings.lmms.path + "/CapturePageVisit";
    }
  }
  eventNames = {
    video_load: "Video Load",
    percent_played: "Percent played",
    start: "Media Begin",
    seek_start: "Seek start",
    seek_end: "Seek end",
    play: "Media Play",
    pause: "Media Pause",
    error: "Error",
    fullscreen_exit: "Fullscreen Exited",
    fullscreen_enter: "Fullscreen Entered",
    resize: "Resize",
    volume_change: "Volume Change",
    player_load: "Player Load",
    end: "Media Complete",
  };
  getEventLabel = function (eventAction, href) {
    var evtLabel;
    switch (eventAction) {
      case "VID - Social click":
      case "VID - CTA click":
        evtLabel =
          player.mediainfo.name + " | " + player.mediainfo.id + " | " + href;
        break;
      default:
        evtLabel = player.mediainfo.name + " | " + player.mediainfo.id;
        break;
    }
    return evtLabel;
  };
  getEventName = function (name) {
    if (options.eventNames && options.eventNames[name]) {
      return options.eventNames[name];
    }
    if (dataSetupOptions.eventNames && dataSetupOptions.eventNames[name]) {
      return dataSetupOptions.eventNames[name];
    }
    if (eventNames[name]) {
      return eventNames[name];
    }
    return name;
  };
  if (
    window.location.host === "players.brightcove.net" ||
    window.location.host === "preview-players.brightcove.net" ||
    trackerName !== ""
  ) {
    tracker = options.tracker || dataSetupOptions.tracker;
    if (tracker) {
      (function (i, s, o, g, r, a, m) {
        i["GoogleAnalyticsObject"] = r;
        i[r] =
          i[r] ||
          function () {
            return (i[r].q = i[r].q || []).push(arguments);
          };
        i[r].l = 1 * new Date();
        a = s.createElement(o);
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        return m.parentNode.insertBefore(a, m);
      })(
        window,
        document,
        "script",
        "//www.google-analytics.com/analytics.js",
        "ga"
      );
      ga("create", tracker, "auto", options.trackerName);
      ga(trackerName + "require", "displayfeatures");
    }
  }
  adStateRegex = /(\s|^)vjs-ad-(playing|loading)(\s|$)/;
  isInAdState = function (player) {
    return adStateRegex.test(player.el().className);
  };
  loaded = function () {
    if (!isInAdState(player)) {
      if (defaultLabel) {
        eventLabel = defaultLabel;
      } else {
        if (player.mediainfo && player.mediainfo.id) {
          eventLabel = player.mediainfo.id + " | " + player.mediainfo.name;
        } else {
          eventLabel = this.currentSrc()
            .split("/")
            .slice(-1)[0]
            .replace(/\.(\w{3,4})(\?.*)?$/i, "");
        }
      }
      if (
        player.mediainfo &&
        player.mediainfo.id &&
        player.mediainfo.id !== currentVideo
      ) {
        currentVideo = player.mediainfo.id;
        percentsAlreadyTracked = [];
        startTracked = false;
        endTracked = false;
        seekStart = seekEnd = 0;
        seeking = false;
        if (__indexOf.call(eventsToTrack, "video_load") >= 0) {
          sendbeacon(
            "asset engagement",
            "VID - Player Load",
            getEventLabel("VID - Player Load"),
            true,
            2
          );
        }
      }
    }
  };
  timeupdate = function () {
    var currentTime, duration, percent, percentPlayed, _i;
    if (!isInAdState(player)) {
      currentTime = Math.round(this.currentTime());
      duration = Math.round(this.duration());
      percentPlayed = Math.round((currentTime / duration) * 100);
      for (
        percent = _i = 0;
        _i <= 100;
        percent = _i += percentsPlayedInterval
      ) {
        if (
          percentPlayed >= percent &&
          __indexOf.call(percentsAlreadyTracked, percent) < 0
        ) {
          if (
            __indexOf.call(eventsToTrack, "percent_played") >= 0 &&
            percentPlayed !== 0 &&
            percent !== 0
          ) {
            sendbeacon(
              "asset engagement",
              "VID - Milestone",
              getEventLabel("VID - Milestone"),
              true,
              percent
            );
          }
          if (percentPlayed > 0) {
            percentsAlreadyTracked.push(percent);
          }
        }
      }
      if (__indexOf.call(eventsToTrack, "seek") >= 0) {
        seekStart = seekEnd;
        seekEnd = currentTime;
        if (seekStart - seekEnd < -1) {
          sendbeacon(
            "asset engagement",
            "VID - Scrubber Forward",
            getEventLabel("VID - Scrubber Forward"),
            false,
            currentTime
          );
        } else if (seekStart - seekEnd > 1) {
          sendbeacon(
            "asset engagement",
            "VID - Scrubber Back",
            getEventLabel("VID - Scrubber Forward"),
            false,
            currentTime
          );
        }
        if (Math.abs(seekStart - seekEnd) > 1) {
          seeking = true;
          //sendbeacon(getEventName('seek_start'), false, seekStart);
          //sendbeacon(getEventName('seek_end'), false, seekEnd);
        }
      }
    }
  };
  rateChange = function () {
    sendbeacon(
      "asset engagement",
      "VID - Speed Change",
      this.mediainfo.name + " | " + this.mediainfo.id,
      false,
      this.playbackRate() * 10
    );
  };
  end = function () {
    if (!isInAdState(player)) {
      sendbeacon(
        "interact",
        "VID - Complete",
        getEventLabel("VID - Complete"),
        true,
        2
      );
      seekStart = seekEnd = 0;
      paused = false;
      //endTracked = true;
    }
  };
  play = function () {
    var currentTime;
    if (!isInAdState(player)) {
      currentTime = Math.round(this.currentTime());
      if (paused) {
        sendbeacon(
          "asset engagement",
          "VID - Resume",
          getEventLabel("VID - Resume"),
          false,
          Math.round((player.currentTime() / player.duration()) * 100)
        );
      } else {
        sendbeacon(
          "interact",
          "VID - Play",
          getEventLabel("VID - Play"),
          false,
          2
        );
      }
      sendRequestToLMMS({
        evt: "Play",
      });
      paused = false;
      seeking = false;
    }
  };
  start = function () {
    if (!isInAdState(player)) {
      if (__indexOf.call(eventsToTrack, "start") >= 0 && !startTracked) {
        //sendbeacon(getEventName('start'), true);
        return (startTracked = true);
      }
    }
  };
  pause = function () {
    var currentTime, duration;
    paused = true;
    if (!isInAdState(player)) {
      currentTime = Math.round(this.currentTime());
      duration = Math.round(this.duration());
      if (currentTime !== duration && !seeking) {
        sendbeacon(
          "asset engagement",
          "VID - Pause",
          getEventLabel("VID - Pause"),
          false,
          Math.round((player.currentTime() / player.duration()) * 100)
        );
      }
    }
  };
  volumeChange = function () {
    var volume;
    volume = this.muted() === true ? 0 : this.volume();
    sendbeacon(
      "asset engagement",
      "VID - Volume Change",
      getEventLabel("VID - Volume Change"),
      false,
      Math.floor(volume * 100)
    );
  };
  resize = function () {
    //sendbeacon(getEventName('resize') + ' - ' + this.width() + "*" + this.height(), true);
  };
  error = function () {
    var currentTime;
    currentTime = Math.round(this.currentTime());
    sendbeacon(
      "asset engagement",
      "VID - Error",
      getEventLabel("VID - Error"),
      true,
      currentTime
    );
  };
  fullscreen = function () {
    var currentTime;
    currentTime = Math.round(this.currentTime());
    if (this.isFullscreen()) {
      sendbeacon(
        "asset engagement",
        "VID - Fullscreen click",
        getEventLabel("VID - Fullscreen click"),
        false,
        2
      );
    } else {
      //sendbeacon(getEventName('fullscreen_exit'), false, currentTime);
    }
  };
  shareButtonClick = function () {
    sendbeacon(
      "asset engagement",
      "VID - Share click",
      getEventLabel("VID - Share click"),
      false,
      2
    );
  };
  shareLinkClick = function (evt) {
    if (
      ["Facebook", "Google+", "LinkedIn", "Twitter"].indexOf(evt.target.title) >
      -1
    ) {
      sendbeacon(
        "asset engagement",
        "VID - Social click",
        getEventLabel("VID - Social click", evt.target.href),
        false,
        2
      );
    }
    if (evt.target.textContent === "Restart") replay();
  };
  ctaClick = function (evt) {
    var aTag = evt.currentTarget.getElementsByTagName("a")[0],
      link = aTag.getAttribute("href");
    sendbeacon(
      "interact",
      "VID - CTA click",
      getEventLabel("VID - CTA click", link),
      false,
      2
    );
  };
  replay = function () {
    sendbeacon(
      "asset engagement",
      "VID - Replay",
      getEventLabel("VID - Replay"),
      false,
      2
    );
  };
  sendbeacon = function (
    eventCategory,
    action,
    eventLabel,
    nonInteraction,
    value
  ) {
    if (typeof Drupal !== "undefined") {
      if (Drupal && Drupal.pegaAnalytics && Drupal.pegaAnalytics.gaSendEvent) {
        Drupal.pegaAnalytics.gaSendEvent(
          eventCategory,
          action,
          eventLabel,
          value,
          nonInteraction
        );
      }
    }
    if (sendbeaconOverride) {
      sendbeaconOverride(
        eventCategory,
        action,
        eventLabel,
        value,
        nonInteraction
      );
    } else if (window.ga) {
      ga(trackerName + "send", "event", {
        eventCategory: eventCategory,
        eventAction: action,
        eventLabel: eventLabel,
        eventValue: value,
        nonInteraction: nonInteraction,
      });
    } else if (window._gaq) {
      _gaq.push([
        "_trackEvent",
        eventCategory,
        action,
        eventLabel,
        value,
        nonInteraction,
      ]);
    } else if (options.debug) {
      videojs.log("Google Analytics not detected");
    }
  };
  sendRequestToLMMS = function (opts) {
    if (typeof lmms == "undefined" || typeof opts == "undefined" || !opts.evt) {
      return;
    }
    var xhr = new XMLHttpRequest(),
      getFormattedDate = function (date) {
        var yyyy = date.getUTCFullYear(),
          mm = date.getUTCMonth() + 1,
          dd = date.getUTCDate(),
          hh = date.getUTCHours(),
          min = date.getUTCMinutes(),
          ss = date.getUTCSeconds();
        if (mm < 10) mm = "0" + mm;
        if (dd < 10) dd = "0" + dd;
        if (hh < 10) hh = "0" + hh;
        if (min < 10) min = "0" + min;
        if (ss < 10) ss = "0" + ss;
        return String(yyyy) + mm + dd + "T" + hh + min + ss + ".000 GMT";
      },
      getParameterByName = function (name) {
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return "";
        return decodeURIComponent(results[2].replace(/\+/g, " "));
      },
      getClosestAttribute = function (elem, selector, attribute) {
        // Element.matches() polyfill
        if (!Element.prototype.matches) {
          Element.prototype.matches =
            Element.prototype.matchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector ||
            Element.prototype.oMatchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            function (s) {
              var matches = (
                  this.document || this.ownerDocument
                ).querySelectorAll(s),
                i = matches.length;
              while (--i >= 0 && matches.item(i) !== this) {}
              return i > -1;
            };
        }

        // Get the closest matching element
        for (; elem && elem !== document; elem = elem.parentNode) {
          if (elem.matches(selector)) {
            if (elem.hasAttribute(attribute)) {
              return elem.getAttribute(attribute);
            }
          }
        }
        return "";
      },
      data = {
        Activity: {
          ASSET_ID: player.mediainfo.id,
          ASSET_NAME: player.el_.getAttribute("data-pega-cd-id"),
          ASSETDISPLAYNAME: player.mediainfo.name,
          ASSET_TYPE:
            player.el_.getAttribute("data-pega-video-asset-type") || "Video",
          ACTIVITY_TYPE:
            player.el_.getAttribute("data-pega-video-type") || "Video",
          ACTIVITY_SUB_TYPE: opts.evt,
          URL: window.location.href,
          UTM_SOURCE: getParameterByName("utm_source") || "",
          UTM_KEYWORD: getParameterByName("utm_keyword") || "",
          UTM_CAMPAIGN: getParameterByName("utm_campaign") || "",
          UTM_MEDIUM: getParameterByName("utm_medium") || "",
          UTM_CONTENT: getParameterByName("utm_content") || "",
          INTERACTION_ID:
            getClosestAttribute(
              player.el_,
              "[data-cdh-interaction-id]",
              "data-cdh-interaction-id"
            ) ||
            getParameterByName("IxID") ||
            "",
          OFFER_ID:
            getClosestAttribute(
              player.el_,
              "[data-cdh-offer-id]",
              "data-cdh-offer-id"
            ) ||
            getParameterByName("O") ||
            "",
          LENGTH: Math.round(player.duration() / 60),
          EVENT_DATE: getFormattedDate(new Date()),
        },
      };
    // Add taxonomy terms if LMMS version supports that method.
    if (
      typeof lmms.getPageTermsFromMetatags === "function" &&
      typeof lmms.getLmmsTermsFromPageTerms === "function"
    ) {
      var terms = lmms.getPageTermsFromMetatags();
      var mapped_terms = lmms.getLmmsTermsFromPageTerms(terms);
      for (var vocab in mapped_terms) {
        data.Activity[vocab] = mapped_terms[vocab];
      }
    }
    data = JSON.stringify(data);
    if (typeof lmmsEndpoint != "undefined") {
      xhr.open("POST", lmmsEndpoint, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
          var json = JSON.parse(xhr.responseText);
          videojs.log(json.Status);
        }
      };
      xhr.send(data);
    }
  };
  this.ready(function () {
    var href, iframe;
    this.on("loadedmetadata", loaded);
    this.on("timeupdate", timeupdate);
    if (__indexOf.call(eventsToTrack, "end") >= 0) {
      this.on("ended", end);
    }
    if (__indexOf.call(eventsToTrack, "play") >= 0) {
      this.on("play", play);
    }
    if (__indexOf.call(eventsToTrack, "start") >= 0) {
      this.on("playing", start);
    }
    if (__indexOf.call(eventsToTrack, "pause") >= 0) {
      this.on("pause", pause);
    }
    if (__indexOf.call(eventsToTrack, "volume_change") >= 0) {
      this.on("volumechange", volumeChange);
    }
    if (__indexOf.call(eventsToTrack, "resize") >= 0) {
      this.on("resize", resize);
    }
    if (__indexOf.call(eventsToTrack, "error") >= 0) {
      this.on("error", error);
    }
    if (__indexOf.call(eventsToTrack, "fullscreen") >= 0) {
      this.on("fullscreenchange", fullscreen);
    }
    this.on("ratechange", rateChange.bind(this));
    if (this.socialButton) {
      this.socialButton.on("click", shareButtonClick);
    }
    if (this.socialOverlay) {
      this.socialOverlay.on("click", shareLinkClick);
    }
    if (player.overlays_ && player.overlays_.length > 0) {
      for (var i = 0; i < player.overlays_.length; i++) {
        player.overlays_[i].on("click", ctaClick);
      }
    }
    // if (__indexOf.call(eventsToTrack, "player_load") >= 0) {
    //   if (self !== top) {
    //     href = document.referrer;
    //     iframe = 1;
    //   } else {
    //     href = window.location.href;
    //     iframe = 0;
    //   }
    //   if (sendbeaconOverride) {
    //     return sendbeaconOverride('asset engagement', 'VID - Player Load', '', 2, true);
    //   } else if (window.ga) {
    //     return ga(trackerName + 'send', 'event', {
    //       'eventCategory': 'asset engagement',
    //       'eventAction': 'VID - Player Load',
    //       'eventLabel': '',
    //       'eventValue': 2,
    //       'nonInteraction': true
    //     });
    //   } else if (window._gaq) {
    //     return _gaq.push(['_trackEvent', 'asset engagement', 'VID - Player Load', '', 2, false]);
    //   } else {
    //     return videojs.log("Google Analytics not detected");
    //   }
    // }
  });
  return {
    sendbeacon: sendbeacon,
  };
});
