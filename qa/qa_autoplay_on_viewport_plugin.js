videojs.registerPlugin("autoplayOnViewport", function (pluginOptions) {
  var player = this;

  const onIntersection = (entries) => {
    for (const entry of entries) {
      if (entry.isIntersecting) {
        player.play();
      } else {
        player.pause();
      }
    }
  };

  setTimeout(function () {
    const observer = new IntersectionObserver(onIntersection, {
      root: null,
      rootMargin: "0px",
      threshold: 0.1,
    });
    observer.observe(document.getElementById(player.id_));
  }, 0);
});
