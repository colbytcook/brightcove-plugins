videojs.registerPlugin("customError", function (message) {
  var player = this;
  if (message) {
    player.el_.querySelector(".vjs-errors-headline").innerHTML = message;
  }
});
